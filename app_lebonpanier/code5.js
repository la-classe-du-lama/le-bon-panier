gdjs.infos2Code = {};
gdjs.infos2Code.localVariables = [];
gdjs.infos2Code.GDfondObjects1= [];
gdjs.infos2Code.GDfondObjects2= [];
gdjs.infos2Code.GDbbtxt_9595info_95951Objects1= [];
gdjs.infos2Code.GDbbtxt_9595info_95951Objects2= [];
gdjs.infos2Code.GDfond_9595jauneObjects1= [];
gdjs.infos2Code.GDfond_9595jauneObjects2= [];
gdjs.infos2Code.GDinfos_95951Objects1= [];
gdjs.infos2Code.GDinfos_95951Objects2= [];
gdjs.infos2Code.GDbbtxt_9595info_95952Objects1= [];
gdjs.infos2Code.GDbbtxt_9595info_95952Objects2= [];
gdjs.infos2Code.GDbouton_9595reculerObjects1= [];
gdjs.infos2Code.GDbouton_9595reculerObjects2= [];
gdjs.infos2Code.GDtxt_9595pageObjects1= [];
gdjs.infos2Code.GDtxt_9595pageObjects2= [];
gdjs.infos2Code.GDbbtxt_9595info_95953Objects1= [];
gdjs.infos2Code.GDbbtxt_9595info_95953Objects2= [];
gdjs.infos2Code.GDpanier_9595Objects1= [];
gdjs.infos2Code.GDpanier_9595Objects2= [];
gdjs.infos2Code.GDoeuf_9595bObjects1= [];
gdjs.infos2Code.GDoeuf_9595bObjects2= [];
gdjs.infos2Code.GDoeuf_9595aObjects1= [];
gdjs.infos2Code.GDoeuf_9595aObjects2= [];
gdjs.infos2Code.GDcarteObjects1= [];
gdjs.infos2Code.GDcarteObjects2= [];
gdjs.infos2Code.GDtxt_9595oeuf_95951Objects1= [];
gdjs.infos2Code.GDtxt_9595oeuf_95951Objects2= [];
gdjs.infos2Code.GDtxt_9595oeuf_95952Objects1= [];
gdjs.infos2Code.GDtxt_9595oeuf_95952Objects2= [];
gdjs.infos2Code.GDbouton_9595suivantObjects1= [];
gdjs.infos2Code.GDbouton_9595suivantObjects2= [];
gdjs.infos2Code.GDswitch_9595couleurObjects1= [];
gdjs.infos2Code.GDswitch_9595couleurObjects2= [];
gdjs.infos2Code.GDtxt_9595etObjects1= [];
gdjs.infos2Code.GDtxt_9595etObjects2= [];
gdjs.infos2Code.GDscore_9595Objects1= [];
gdjs.infos2Code.GDscore_9595Objects2= [];
gdjs.infos2Code.GDtxt_9595couleurObjects1= [];
gdjs.infos2Code.GDtxt_9595couleurObjects2= [];
gdjs.infos2Code.GDpouletObjects1= [];
gdjs.infos2Code.GDpouletObjects2= [];
gdjs.infos2Code.GDbouton_9595retourObjects1= [];
gdjs.infos2Code.GDbouton_9595retourObjects2= [];


gdjs.infos2Code.mapOfGDgdjs_9546infos2Code_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infos2Code.GDbouton_9595retourObjects1});
gdjs.infos2Code.mapOfGDgdjs_9546infos2Code_9546GDbouton_95959595reculerObjects1Objects = Hashtable.newFrom({"bouton_reculer": gdjs.infos2Code.GDbouton_9595reculerObjects1});
gdjs.infos2Code.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond"), gdjs.infos2Code.GDfondObjects1);
gdjs.copyArray(runtimeScene.getObjects("fond_jaune"), gdjs.infos2Code.GDfond_9595jauneObjects1);
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}{for(var i = 0, len = gdjs.infos2Code.GDfondObjects1.length ;i < len;++i) {
    gdjs.infos2Code.GDfondObjects1[i].getBehavior("Opacity").setOpacity(100);
}
}{for(var i = 0, len = gdjs.infos2Code.GDfond_9595jauneObjects1.length ;i < len;++i) {
    gdjs.infos2Code.GDfond_9595jauneObjects1[i].getBehavior("Opacity").setOpacity(150);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infos2Code.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infos2Code.mapOfGDgdjs_9546infos2Code_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") >= 0.5;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_reculer"), gdjs.infos2Code.GDbouton_9595reculerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infos2Code.mapOfGDgdjs_9546infos2Code_9546GDbouton_95959595reculerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "infos", false);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs.infos2Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infos2Code.GDfondObjects1.length = 0;
gdjs.infos2Code.GDfondObjects2.length = 0;
gdjs.infos2Code.GDbbtxt_9595info_95951Objects1.length = 0;
gdjs.infos2Code.GDbbtxt_9595info_95951Objects2.length = 0;
gdjs.infos2Code.GDfond_9595jauneObjects1.length = 0;
gdjs.infos2Code.GDfond_9595jauneObjects2.length = 0;
gdjs.infos2Code.GDinfos_95951Objects1.length = 0;
gdjs.infos2Code.GDinfos_95951Objects2.length = 0;
gdjs.infos2Code.GDbbtxt_9595info_95952Objects1.length = 0;
gdjs.infos2Code.GDbbtxt_9595info_95952Objects2.length = 0;
gdjs.infos2Code.GDbouton_9595reculerObjects1.length = 0;
gdjs.infos2Code.GDbouton_9595reculerObjects2.length = 0;
gdjs.infos2Code.GDtxt_9595pageObjects1.length = 0;
gdjs.infos2Code.GDtxt_9595pageObjects2.length = 0;
gdjs.infos2Code.GDbbtxt_9595info_95953Objects1.length = 0;
gdjs.infos2Code.GDbbtxt_9595info_95953Objects2.length = 0;
gdjs.infos2Code.GDpanier_9595Objects1.length = 0;
gdjs.infos2Code.GDpanier_9595Objects2.length = 0;
gdjs.infos2Code.GDoeuf_9595bObjects1.length = 0;
gdjs.infos2Code.GDoeuf_9595bObjects2.length = 0;
gdjs.infos2Code.GDoeuf_9595aObjects1.length = 0;
gdjs.infos2Code.GDoeuf_9595aObjects2.length = 0;
gdjs.infos2Code.GDcarteObjects1.length = 0;
gdjs.infos2Code.GDcarteObjects2.length = 0;
gdjs.infos2Code.GDtxt_9595oeuf_95951Objects1.length = 0;
gdjs.infos2Code.GDtxt_9595oeuf_95951Objects2.length = 0;
gdjs.infos2Code.GDtxt_9595oeuf_95952Objects1.length = 0;
gdjs.infos2Code.GDtxt_9595oeuf_95952Objects2.length = 0;
gdjs.infos2Code.GDbouton_9595suivantObjects1.length = 0;
gdjs.infos2Code.GDbouton_9595suivantObjects2.length = 0;
gdjs.infos2Code.GDswitch_9595couleurObjects1.length = 0;
gdjs.infos2Code.GDswitch_9595couleurObjects2.length = 0;
gdjs.infos2Code.GDtxt_9595etObjects1.length = 0;
gdjs.infos2Code.GDtxt_9595etObjects2.length = 0;
gdjs.infos2Code.GDscore_9595Objects1.length = 0;
gdjs.infos2Code.GDscore_9595Objects2.length = 0;
gdjs.infos2Code.GDtxt_9595couleurObjects1.length = 0;
gdjs.infos2Code.GDtxt_9595couleurObjects2.length = 0;
gdjs.infos2Code.GDpouletObjects1.length = 0;
gdjs.infos2Code.GDpouletObjects2.length = 0;
gdjs.infos2Code.GDbouton_9595retourObjects1.length = 0;
gdjs.infos2Code.GDbouton_9595retourObjects2.length = 0;

gdjs.infos2Code.eventsList0(runtimeScene);

return;

}

gdjs['infos2Code'] = gdjs.infos2Code;
