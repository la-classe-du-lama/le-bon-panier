gdjs.intro_95remerciementsCode = {};
gdjs.intro_95remerciementsCode.localVariables = [];
gdjs.intro_95remerciementsCode.GDintroObjects1= [];
gdjs.intro_95remerciementsCode.GDintroObjects2= [];
gdjs.intro_95remerciementsCode.GDoeuf_9595bObjects1= [];
gdjs.intro_95remerciementsCode.GDoeuf_9595bObjects2= [];
gdjs.intro_95remerciementsCode.GDoeuf_9595aObjects1= [];
gdjs.intro_95remerciementsCode.GDoeuf_9595aObjects2= [];
gdjs.intro_95remerciementsCode.GDcarteObjects1= [];
gdjs.intro_95remerciementsCode.GDcarteObjects2= [];
gdjs.intro_95remerciementsCode.GDtxt_9595oeuf_95951Objects1= [];
gdjs.intro_95remerciementsCode.GDtxt_9595oeuf_95951Objects2= [];
gdjs.intro_95remerciementsCode.GDtxt_9595oeuf_95952Objects1= [];
gdjs.intro_95remerciementsCode.GDtxt_9595oeuf_95952Objects2= [];
gdjs.intro_95remerciementsCode.GDbouton_9595suivantObjects1= [];
gdjs.intro_95remerciementsCode.GDbouton_9595suivantObjects2= [];
gdjs.intro_95remerciementsCode.GDswitch_9595couleurObjects1= [];
gdjs.intro_95remerciementsCode.GDswitch_9595couleurObjects2= [];
gdjs.intro_95remerciementsCode.GDtxt_9595etObjects1= [];
gdjs.intro_95remerciementsCode.GDtxt_9595etObjects2= [];
gdjs.intro_95remerciementsCode.GDscore_9595Objects1= [];
gdjs.intro_95remerciementsCode.GDscore_9595Objects2= [];
gdjs.intro_95remerciementsCode.GDtxt_9595couleurObjects1= [];
gdjs.intro_95remerciementsCode.GDtxt_9595couleurObjects2= [];
gdjs.intro_95remerciementsCode.GDpouletObjects1= [];
gdjs.intro_95remerciementsCode.GDpouletObjects2= [];
gdjs.intro_95remerciementsCode.GDbouton_9595retourObjects1= [];
gdjs.intro_95remerciementsCode.GDbouton_9595retourObjects2= [];


gdjs.intro_95remerciementsCode.mapOfGDgdjs_9546intro_959595remerciementsCode_9546GDbouton_95959595suivantObjects1Objects = Hashtable.newFrom({"bouton_suivant": gdjs.intro_95remerciementsCode.GDbouton_9595suivantObjects1});
gdjs.intro_95remerciementsCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_suivant"), gdjs.intro_95remerciementsCode.GDbouton_9595suivantObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.intro_95remerciementsCode.mapOfGDgdjs_9546intro_959595remerciementsCode_9546GDbouton_95959595suivantObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.intro_95remerciementsCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.intro_95remerciementsCode.GDintroObjects1.length = 0;
gdjs.intro_95remerciementsCode.GDintroObjects2.length = 0;
gdjs.intro_95remerciementsCode.GDoeuf_9595bObjects1.length = 0;
gdjs.intro_95remerciementsCode.GDoeuf_9595bObjects2.length = 0;
gdjs.intro_95remerciementsCode.GDoeuf_9595aObjects1.length = 0;
gdjs.intro_95remerciementsCode.GDoeuf_9595aObjects2.length = 0;
gdjs.intro_95remerciementsCode.GDcarteObjects1.length = 0;
gdjs.intro_95remerciementsCode.GDcarteObjects2.length = 0;
gdjs.intro_95remerciementsCode.GDtxt_9595oeuf_95951Objects1.length = 0;
gdjs.intro_95remerciementsCode.GDtxt_9595oeuf_95951Objects2.length = 0;
gdjs.intro_95remerciementsCode.GDtxt_9595oeuf_95952Objects1.length = 0;
gdjs.intro_95remerciementsCode.GDtxt_9595oeuf_95952Objects2.length = 0;
gdjs.intro_95remerciementsCode.GDbouton_9595suivantObjects1.length = 0;
gdjs.intro_95remerciementsCode.GDbouton_9595suivantObjects2.length = 0;
gdjs.intro_95remerciementsCode.GDswitch_9595couleurObjects1.length = 0;
gdjs.intro_95remerciementsCode.GDswitch_9595couleurObjects2.length = 0;
gdjs.intro_95remerciementsCode.GDtxt_9595etObjects1.length = 0;
gdjs.intro_95remerciementsCode.GDtxt_9595etObjects2.length = 0;
gdjs.intro_95remerciementsCode.GDscore_9595Objects1.length = 0;
gdjs.intro_95remerciementsCode.GDscore_9595Objects2.length = 0;
gdjs.intro_95remerciementsCode.GDtxt_9595couleurObjects1.length = 0;
gdjs.intro_95remerciementsCode.GDtxt_9595couleurObjects2.length = 0;
gdjs.intro_95remerciementsCode.GDpouletObjects1.length = 0;
gdjs.intro_95remerciementsCode.GDpouletObjects2.length = 0;
gdjs.intro_95remerciementsCode.GDbouton_9595retourObjects1.length = 0;
gdjs.intro_95remerciementsCode.GDbouton_9595retourObjects2.length = 0;

gdjs.intro_95remerciementsCode.eventsList0(runtimeScene);

return;

}

gdjs['intro_95remerciementsCode'] = gdjs.intro_95remerciementsCode;
